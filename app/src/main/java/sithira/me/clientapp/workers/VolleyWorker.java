package sithira.me.clientapp.workers;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import sithira.me.clientapp.interfaces.VolleyResponseListener;

public class VolleyWorker
{
	
	public static void makeJSONObjectRequest(Context context, String url,
	                                         final VolleyResponseListener listener) {
		
		
		JsonObjectRequest request = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>()
		{
			@Override
			public void onResponse(JSONObject response) {
				listener.onResponse(response);
			}
		},
		new Response.ErrorListener()
		{
			@Override
			public void onErrorResponse(VolleyError error) {
				listener.onError(error);
			}
		});
		
		Volley.newRequestQueue(context).add(request);
		
	}
	
}
