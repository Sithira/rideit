package sithira.me.clientapp.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sithira.me.clientapp.helpers.AltDialog;
import sithira.me.clientapp.interfaces.VolleyResponseListener;
import sithira.me.clientapp.models.SimilarRoutes;
import sithira.me.clientapp.workers.VolleyWorker;
import sithira.me.clientapp.R;
import sithira.me.clientapp.animators.MapAnimator;

public class OriginDestinationMap extends FragmentActivity implements OnMapReadyCallback
{
	
	private GoogleMap mMap;
	
	private LatLng origin, destination;
	
	private ArrayList<SimilarRoutes> similarRoutes;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_origin_destination_map);
		// Obtain the SupportMapFragment and get notified when the map is ready to be used.
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);
		
		
		Bundle args = getIntent().getParcelableExtra("bundle");
		
		origin = args.getParcelable("origin");
		destination = args.getParcelable("destination");
		
		Toast.makeText(this, "" + origin + " <--> " + destination, Toast.LENGTH_LONG).show();
	}
	
	
	/**
	 * Manipulates the map once available.
	 * This callback is triggered when the map is ready to be used.
	 * This is where we can add markers or lines, add listeners or move the camera. In this case,
	 * we just add a marker near Sydney, Australia.
	 * If Google Play services is not installed on the device, the user will be prompted to install
	 * it inside the SupportMapFragment. This method will only be triggered once the user has
	 * installed Google Play services and returned to the app.
	 */
	@Override
	public void onMapReady(GoogleMap googleMap) {
		
		mMap = googleMap;
		
		if (this.origin != null && this.destination != null)
		{
			
			String url = "https://maps.googleapis.com/maps/api/directions/json?origin="
					+ origin.latitude + "," + origin.longitude
					+ "&destination="
					+ destination.latitude + "," + destination.longitude
					+ "&alternatives=true&key=" + getString(R.string.google_api_key);
			
			VolleyWorker.makeJSONObjectRequest(OriginDestinationMap.this, url, new VolleyResponseListener()
			{
				
				@Override
				public void onResponse(Object response) {
					
					similarRoutes = new ArrayList<>();
					
					JSONObject json = (JSONObject) response;
					
					try
					{
						
						JSONArray routes = json.getJSONArray("routes");
						
						// Loop through all the routes
						for (int i = 0; i < routes.length(); i++)
						{
							
							// get the route from route
							JSONObject route = routes.getJSONObject(i);
							
							// get the first leg
							JSONObject leg = route.getJSONArray("legs")
									.getJSONObject(0);
							
							// get the ETA
							String eta = leg.getJSONObject("duration").getString("text");
							
							// get overview polylines
							String overview_polylines = route.getJSONObject("overview_polyline")
									.getString("points");
							
							// decode and get the List of LatLngs
							List<LatLng> decodedPath = PolyUtil.decode(overview_polylines);
							
							// append to the model
							similarRoutes.add(new SimilarRoutes(origin, destination, eta, decodedPath));
							
						}
						
						// add the polylines to the maps
						for (int i = 0; i < similarRoutes.size(); i++)
						{
							
							// make the custom polylines
							PolylineOptions polylineOptions = new PolylineOptions()
									.clickable(true) // make it clickable
									.addAll(similarRoutes.get(i).getOverviewPolylines())
									.color(R.color.routeOptions); // add the color
							
							// add to maps
							mMap.addPolyline(polylineOptions);
						}
						
						// move the camera
						mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(origin, 15));
						
					} catch (JSONException e)
					{
						new AltDialog().showAlert("JSON Parse Error", e.toString());
					}
					
				}
				
				@Override
				public void onError(Object error) {
					
					new AltDialog().showAlert("Request Error", error.toString());
					
				}
			});
			
			// start listening for clicks
			selectRoute();
			
		}
		else
		{
			new AltDialog().on(this).showAlert("Error", "Origin or Destination was not set");
		}
		
	}
	
	private void selectRoute() {
		
		mMap.setOnPolylineClickListener(new GoogleMap.OnPolylineClickListener()
		{
			@Override
			public void onPolylineClick(Polyline polyline) {
				
				mMap.clear();
				
				List<LatLng> clickedPolyline = polyline.getPoints();
				
				MapAnimator.getInstance().animateRoute(mMap, clickedPolyline);
				
				for (SimilarRoutes route : similarRoutes)
				{
					
					if (route.getOverviewPolylines().containsAll(clickedPolyline))
					{
						
						mMap.addMarker(new MarkerOptions()
								.title("My Destination")
								.snippet("ETA: " + route.getEta())
								.position(destination));
						
					}
					
				}
				
				mMap.setOnPolylineClickListener(null);
				
			}
		});
		
	}
}
