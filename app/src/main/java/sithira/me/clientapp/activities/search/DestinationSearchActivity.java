package sithira.me.clientapp.activities.search;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sithira.me.clientapp.adapters.LocationItemAdapter;
import sithira.me.clientapp.interfaces.VolleyResponseListener;
import sithira.me.clientapp.models.LocationItem;
import sithira.me.clientapp.workers.VolleyWorker;
import sithira.me.clientapp.R;
import sithira.me.clientapp.activities.OriginDestinationMap;

public class DestinationSearchActivity extends AppCompatActivity implements OnMapReadyCallback
{
	
	private GoogleMap mMap;
	
	private SearchView mDestination;
	
	private ListView mListViewDestination;
	
	private ProgressBar mProgressBar;
	
	private LocationItemAdapter mDestinationLocationAdapter;
	
	private List<LocationItem> mDestinationLocationList;
	
	private String destinationPlaceId;
	
	private LinearLayout mMapsLayout;
	
	private LatLng origin, destination;
	
	private Handler mHandler;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_destination_search);
		
		initVariableReferences();
		
		mProgressBar.setVisibility(View.GONE);
		
		// get the latalng
		Bundle args = getIntent().getParcelableExtra("bundle");
		origin = args.getParcelable("origin");
		
		Toast.makeText(this, "" + origin, Toast.LENGTH_LONG).show();
		
		initializeListeners();
		
	}
	
	@Override
	public void onMapReady(GoogleMap googleMap) {
		
		mMap = googleMap;
	}
	
	private void initVariableReferences() {
		
		mDestination = findViewById(R.id.destination);
		
		mListViewDestination = findViewById(R.id.destinations);
		
		mProgressBar = findViewById(R.id.progressBar);
		
		mDestinationLocationList = new ArrayList<>();
		
		mDestinationLocationAdapter = new LocationItemAdapter(DestinationSearchActivity.this, mDestinationLocationList);
		
		mListViewDestination.setAdapter(mDestinationLocationAdapter);
		
		mMapsLayout = findViewById(R.id.mapsLayout);
		
		mMapsLayout.setVisibility(View.GONE);
	}
	
	private void initializeListeners() {
		
		this.mDestination.setOnQueryTextListener(new SearchView.OnQueryTextListener()
		{
			@Override
			public boolean onQueryTextSubmit(String s) {
				return false;
			}
			
			@Override
			public boolean onQueryTextChange(final String s) {
				
				if (mHandler != null)
				{
					mHandler.removeCallbacksAndMessages(null);
				}
				
				if (s.length() >= 3)
				{
					
					System.out.println(s);
					
					mProgressBar.setVisibility(View.GONE);
					
					mHandler = new Handler();
					
					mHandler.postDelayed(new Runnable()
					{
						@Override
						public void run() {
							String url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input="
									+ s +
									"&components=country:lk&key=" + getString(R.string.google_api_key);
							
							VolleyWorker.makeJSONObjectRequest(DestinationSearchActivity.this, url, new VolleyResponseListener()
							{
								@Override
								public void onResponse(Object response) {
									
									JSONObject json = (JSONObject) response;
									
									try
									{
										JSONArray predictions = json.getJSONArray("predictions");
										
										ArrayList<LocationItem> items = new ArrayList<>();
										
										for (int i = 0; i < predictions.length(); i++)
										{
											
											JSONObject jsonObject = predictions.getJSONObject(i);
											
											String location = jsonObject.getJSONObject("structured_formatting")
													.get("main_text").toString();
											
											System.out.println(location);
											
											String location_2 = jsonObject.getJSONObject("structured_formatting")
													.get("secondary_text").toString();
											
											String place_id = jsonObject.get("place_id")
													.toString();
											
											LocationItem item = new LocationItem(location, location_2, place_id);
											
											items.add(item);
											
										}
										
										mDestinationLocationList.addAll(items);
										
										mDestinationLocationAdapter.notifyDataSetChanged();
										
										
									} catch (JSONException e)
									{
										e.printStackTrace();
									}
									
								}
								
								@Override
								public void onError(Object error) {
								
								}
							});
						}
					}, 1000);
				}
				else
				{
					
					if (s.length() == 0)
					{
						mProgressBar.setVisibility(View.GONE);
					}
					else
					{
						mProgressBar.setVisibility(View.VISIBLE);
					}
					
				}

				
				return false;
			}
		});
		
		mListViewDestination.setOnItemClickListener(new AdapterView.OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
			
				LocationItem location = (LocationItem) mDestinationLocationAdapter.getItem(i);
				
				final String url = "https://maps.googleapis.com/maps/api/geocode/json?place_id="
						+ location.getPlaceId() +
						"&key=" + getString(R.string.google_api_key);
				
				VolleyWorker.makeJSONObjectRequest(DestinationSearchActivity.this, url, new VolleyResponseListener()
				{
					@Override
					public void onResponse(Object response) {
						
						JSONObject json = (JSONObject) response;
						
						try
						{
							
							JSONObject place = json.getJSONArray("results")
									.getJSONObject(0)
									.getJSONObject("geometry")
									.getJSONObject("location");
							
							destination = new LatLng(place.getDouble("lat"), place.getDouble("lng"));
							
							Bundle args = new Bundle();
							
							args.putParcelable("origin", origin);
							args.putParcelable("destination", destination);
							
							Intent intent = new Intent(DestinationSearchActivity.this, OriginDestinationMap.class);
							
							intent.putExtra("bundle", args);
							
							startActivity(intent);
							
							
						} catch (JSONException e)
						{
							e.printStackTrace();
						}
						
					}
					
					@Override
					public void onError(Object error) {
					
					}
				});
			
			}
		});
		
	}
	
}
