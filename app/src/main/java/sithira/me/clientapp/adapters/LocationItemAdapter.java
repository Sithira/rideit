package sithira.me.clientapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import sithira.me.clientapp.R;
import sithira.me.clientapp.models.LocationItem;

public class LocationItemAdapter extends BaseAdapter implements View.OnClickListener
{
	Activity mActivity;
	
	List<LocationItem> mList;
	
	private LayoutInflater inflater;
	
	public LocationItemAdapter(Activity mActivity, List<LocationItem> mList) {
		this.mActivity = mActivity;
		this.mList = mList;
		
		
	}
	
	@Override
	public void onClick(View view) {
	
	}
	
	@Override
	public int getCount() {
		return mList.size();
	}
	
	@Override
	public Object getItem(int i) {
		return mList.get(i);
	}
	
	@Override
	public long getItemId(int i) {
		return i;
	}
	
	public void clear()
	{
		mList.clear();
	}
	
	@Override
	public View getView(int i, View view, ViewGroup viewGroup) {
		
		View vi = view;
		
		if (inflater == null)
		{
			inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
		
		if (vi == null)
		{
			vi = inflater.inflate(R.layout.location_item, null);
		}
		
		LocationItem item = mList.get(i);
		
		TextView mMainTextView = (TextView) vi.findViewById(R.id.main_name);
		//
		TextView mSecondryTextView = (TextView) vi.findViewById(R.id.secondry_name);
		
		mMainTextView.setText(item.getMainName());
		
		mSecondryTextView.setText(item.getSecondaryName());
		
		return vi;
	}
}
