package sithira.me.clientapp.helpers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class AltDialog
{
	
	private AlertDialog.Builder builder;
	
	private Context context;
	
	public AltDialog on(Context context)
	{
		this.context = context;
		
		return this;
	}
	
	public void showAlert(String title, String message)
	{
		
		this.builder.setTitle(title);
		
		this.builder.setMessage(message);
		
		this.builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
			
				dialogInterface.dismiss();
				
			}
		});
	}
	
}
