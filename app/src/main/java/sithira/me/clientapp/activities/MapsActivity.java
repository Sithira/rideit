package sithira.me.clientapp.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesWithFallbackProvider;
import sithira.me.clientapp.R;
import sithira.me.clientapp.activities.search.DestinationSearchActivity;
import sithira.me.clientapp.activities.search.OriginSearchActivity;
import sithira.me.clientapp.interfaces.VolleyResponseListener;
import sithira.me.clientapp.workers.VolleyWorker;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback
{
	
	private GoogleMap mMap;
	
	private TextView currentLocationTextView;
	
	private Button mButton;
	
	private LatLng currentLocation;
	
	private LatLng pickUpLocation;
	
	private Handler mHandler;
	private boolean USER_MOVED_CAMERA;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_maps);
		
		this.mButton = findViewById(R.id.here);
		
		this.currentLocationTextView = findViewById(R.id.currentLocation);
		this.currentLocationTextView.setGravity(Gravity.CENTER);
		
		// check for the permissions
		
		final Context context = this;
		
		Dexter.withActivity(this).withPermissions(
				Manifest.permission.INTERNET,
				Manifest.permission.ACCESS_FINE_LOCATION,
				Manifest.permission.ACCESS_COARSE_LOCATION)
				.withListener(new MultiplePermissionsListener()
				{
					@Override
					public void onPermissionsChecked(MultiplePermissionsReport report) {
						
						if (report.areAllPermissionsGranted())
						{
							Toast.makeText(context, "All Permissions Granted, Thanks", Toast.LENGTH_LONG)
									.show();
							
							return;
						}
						
						for (PermissionDeniedResponse deniedResponse : report.getDeniedPermissionResponses())
						{
							
							return;
						}
					}
					
					@Override
					public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
						token.continuePermissionRequest();
					}
				})
				.check();
		
		// Obtain the SupportMapFragment and get notified when the map is ready to be used.
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);
		
		this.currentLocationTextView.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view) {
				
				Intent searchIntent = new Intent(MapsActivity.this, OriginSearchActivity.class);
				
				searchIntent.putExtra("currentLocation", currentLocationTextView.getText());
				
				startActivity(searchIntent);
				
			}
		});
		
		
		mButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view) {
				
				Intent destinationIntent = new Intent(MapsActivity.this, DestinationSearchActivity.class);
				
				Bundle args = new Bundle();
				
				args.putParcelable("origin", pickUpLocation);
				
				destinationIntent.putExtra("bundle", args);
				
				startActivity(destinationIntent);
				
			}
		});
		
	}
	
	
	@Override
	public void onMapReady(GoogleMap googleMap) {
		mMap = googleMap;
		
		
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
		{
			
			// TODO: Consider calling
			//    ActivityCompat#requestPermissions
			// here to request the missing permissions, and then overriding
			//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
			//                                          int[] grantResults)
			// to handle the case where the user grants the permission. See the documentation
			// for ActivityCompat#requestPermissions for more details.
			return;
		}
		
		// initialize location updates
		initializeSmartLocation();
		
		mMap.setMyLocationEnabled(true);
		
		getLastKnownLocation();
		
		if (currentLocation != null)
		{
			getLocationName(currentLocation);
		}
		
		cameraMoveListener();
	}
	
	private void initializeSmartLocation()
	{
		SmartLocation.with(this).location().start(new OnLocationUpdatedListener()
		{
			@Override
			public void onLocationUpdated(Location location) {
				
				
				LatLng currentPosition = new LatLng(location.getLatitude(), location.getLongitude());
				
				CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(currentPosition, 16);
				
				mMap.moveCamera(cameraUpdate);
				
				System.out.println("Lats:" + location.getLatitude() + " Lng: " + location.getLongitude());
				
				// two assignments
				currentLocation = pickUpLocation = currentPosition;
				
			}
		});
	}
	
	/**
	 * Get the last location
	 */
	public void getLastKnownLocation() {
		
		LocationGooglePlayServicesWithFallbackProvider provider =
				new LocationGooglePlayServicesWithFallbackProvider(this);
		
		Location mLocation = SmartLocation.with(this)
				.location()
				.getLastLocation();
		
		if (mLocation == null)
		{
			Toast.makeText(this, "Whoops, No Location Details", Toast.LENGTH_LONG)
					.show();
			
			return;
		}
		
		System.out.println("----> Lats:" + mLocation.getLatitude() + " Lng: " + mLocation.getLongitude());
		
		currentLocation = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
		
		updateCameraView(currentLocation);
	}
	
	/**
	 * Update the view of the map when correct latlng is provided
	 *
	 * @param latLng
	 */
	private void updateCameraView(LatLng latLng) {
		
		mMap.clear();
		
//		// Add a marker in Sydney and move the camera
//		LatLng sydney = new LatLng(latLng.latitude, latLng.longitude);
//		mMap.addMarker(new MarkerOptions().position(sydney).title("My Location"));
//		mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
	}
	
	/**
	 * Get the name of location
	 *
	 * @param latLng
	 * @return
	 */
	private void getLocationName(LatLng latLng) {
		
		String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="
				+ latLng.latitude + "," + latLng.longitude +
				"&key=" + getString(R.string.google_api_key);
		
		VolleyWorker.makeJSONObjectRequest(this, url, new VolleyResponseListener()
		{
			
			@Override
			public void onResponse(Object response) {
				
				JSONObject resp = (JSONObject) response;
				
				try
				{
					// System.out.println(resp.toString(4));
					
					String formatted_address = resp.getJSONArray("results").getJSONObject(0).getString("formatted_address");
					
					currentLocationTextView.setText(formatted_address);
					
				} catch (JSONException e)
				{
					e.printStackTrace();
				}
			}
			
			@Override
			public void onError(Object error) {
			
			}
		});
	}
	
	private void cameraMoveListener() {
		
		
		mMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener()
		{
			@Override
			public void onCameraMoveStarted(int i) {
				
				
				if (i == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE)
				{
					USER_MOVED_CAMERA = true;
					
					currentLocationTextView.setText(R.string.loading_text);
				}
				
			}
		});
		
		mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener()
		{
			@Override
			public void onCameraIdle() {
				
				if (USER_MOVED_CAMERA)
				{
					final LatLng newTarget = mMap.getCameraPosition().target;
					
					if (mHandler != null)
					{
						mHandler.removeCallbacksAndMessages(null);
					}
					
					mHandler = new Handler();
					
					mHandler.postDelayed(new Runnable()
					{
						@Override
						public void run() {
							
							pickUpLocation = newTarget;
							
							System.out.println("Fire");
							
							updateCameraView(newTarget);
							
							getLocationName(newTarget);
							
							USER_MOVED_CAMERA = false;
							
							
						}
					}, 1500);
					
				}
				
			}
		});
		
	}
}
