package sithira.me.clientapp.models;

public class LocationItem
{
	
	private String mainName, secondaryName, placeId;
	
	private double latitude, longitude;
	
	public LocationItem(String mainName, String secondaryName, String placeId) {
		this.mainName = mainName;
		this.secondaryName = secondaryName;
		this.placeId = placeId;
	}
	
	public LocationItem(String mainName, String secondaryName, String placeId, double latitude, double longitude) {
		this.mainName = mainName;
		this.secondaryName = secondaryName;
		this.placeId = placeId;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	public String getMainName() {
		return mainName;
	}
	
	public String getSecondaryName() {
		return secondaryName;
	}
	
	public String getPlaceId() {
		return placeId;
	}
	
}
