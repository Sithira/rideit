package sithira.me.clientapp.interfaces;

import com.google.android.gms.maps.model.LatLng;

public interface VolleyResponseListener
{
	
	void onResponse(Object response);
	
	void onError(Object error);
	
}
