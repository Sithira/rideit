package sithira.me.clientapp.models;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class SimilarRoutes
{
	
	private LatLng origin;
	
	private LatLng destination;
	
	private String eta;
	
	private List<LatLng> overview_polylines;
	
	public SimilarRoutes(LatLng origin, LatLng destination, String eta, List<LatLng> overview_polylines)
	{
		this.origin = origin;
		this.destination = destination;
		this.eta = eta;
		this.overview_polylines = overview_polylines;
	}
	
	public String getEta()
	{
		return eta;
	}
	
	public LatLng getLatitude()
	{
		return origin;
	}
	
	public LatLng getLongitude()
	{
		return destination;
	}
	
	public List<LatLng> getOverviewPolylines()
	{
		return overview_polylines;
	}
}
