package sithira.me.clientapp.Start;

import android.content.Intent;

import com.daimajia.androidanimations.library.Techniques;
import com.viksaa.sssplash.lib.activity.AwesomeSplash;
import com.viksaa.sssplash.lib.cnst.Flags;
import com.viksaa.sssplash.lib.model.ConfigSplash;

import sithira.me.clientapp.R;
import sithira.me.clientapp.activities.MapsActivity;

public class SplashScreen extends AwesomeSplash
{
	
	@Override
	public void initSplash(ConfigSplash configSplash) {
		//Customize Circular Reveal
		configSplash.setBackgroundColor(R.color.colorPrimary); //any color you want form colors.xml
		configSplash.setAnimCircularRevealDuration(1000); //int ms
		configSplash.setRevealFlagX(Flags.REVEAL_RIGHT);  //or Flags.REVEAL_LEFT
		configSplash.setRevealFlagY(Flags.REVEAL_BOTTOM); //or Flags.REVEAL_TOP
		
		//Choose LOGO OR PATH; if you don't provide String value for path it's logo by default
		
		//Customize Logo
		configSplash.setLogoSplash(R.mipmap.ic_launcher); //or any other drawable
		configSplash.setAnimLogoSplashDuration(100); //int ms
		configSplash.setAnimLogoSplashTechnique(Techniques.Bounce); //choose one form Techniques (ref: https://github.com/daimajia/AndroidViewAnimations)
		
		
		//Customize Path
		//configSplash.setPathSplash(Constants.DROID_LOGO); //set path String
		configSplash.setOriginalHeight(400); //in relation to your svg (path) resource
		configSplash.setOriginalWidth(400); //in relation to your svg (path) resource
		configSplash.setAnimPathStrokeDrawingDuration(100);
		configSplash.setPathSplashStrokeSize(3); //I advise value be <5
		configSplash.setPathSplashStrokeColor(R.color.colorWhite); //any color you want form colors.xml
		configSplash.setAnimPathFillingDuration(100);
		configSplash.setPathSplashFillColor(R.color.colorYellow); //path object filling color
		
		
		//Customize Title
		configSplash.setTitleSplash("RIDEIT.");
		configSplash.setTitleTextColor(R.color.colorWhite);
		configSplash.setTitleTextSize(30f); //float value
		configSplash.setAnimTitleDuration(3000);
		configSplash.setAnimTitleTechnique(Techniques.FadeIn);
	}
	
	@Override
	public void animationsFinished() {
		Intent intent = new Intent(this, MapsActivity.class);
		startActivity(intent);
		finish();
		
	}
}
