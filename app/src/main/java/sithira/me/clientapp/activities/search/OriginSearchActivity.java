package sithira.me.clientapp.activities.search;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sithira.me.clientapp.adapters.LocationItemAdapter;
import sithira.me.clientapp.interfaces.VolleyResponseListener;
import sithira.me.clientapp.models.LocationItem;
import sithira.me.clientapp.workers.VolleyWorker;
import sithira.me.clientapp.R;
import sithira.me.clientapp.activities.MapsActivity;

public class OriginSearchActivity extends AppCompatActivity implements OnMapReadyCallback
{
	
	private GoogleMap mMap;
	
	private SearchView mOrigin;
	
	private ListView mListViewOrigin;
	
	private ProgressBar mProgressBar;
	
	private LocationItemAdapter mOriginLocationAdapter;
	
	private List<LocationItem> mOriginLocationList;
	
	private String originPlaceId;
	
	private LinearLayout mMapsLayout;
	
	private LatLng origin, destination;
	
	private Handler mHandler;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_origin_search);
		
		initVariableReferences();
		
		// hide the progress bar
		mProgressBar.setVisibility(View.GONE);
		
		initializeSearchListeners();
		
		listViewClickListeners();
	}
	
	/**
	 * Initializing variable references
	 */
	private void initVariableReferences() {
		// init the text boxes
		mOrigin = findViewById(R.id.origin);
		
		// init the progress
		mProgressBar = findViewById(R.id.progressBar);
		
		// init the list views
		mListViewOrigin = findViewById(R.id.origins);
		
		mMapsLayout = findViewById(R.id.mapsLayout);
		
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.originMap);
		mapFragment.getMapAsync(this);
		
		// lists
		mOriginLocationList = new ArrayList<>();
		
		// init adapters
		mOriginLocationAdapter = new LocationItemAdapter(this, mOriginLocationList);
		
		// set the adapters
		mListViewOrigin.setAdapter(mOriginLocationAdapter);
	}
	
	private void initializeSearchListeners() {
		
		this.mOrigin.setOnQueryTextListener(new SearchView.OnQueryTextListener()
		{
			@Override
			public boolean onQueryTextSubmit(String s) {
				return false;
			}
			
			@Override
			public boolean onQueryTextChange(final String s) {
				
				mListViewOrigin.setVisibility(View.VISIBLE);
				
				mProgressBar.setVisibility(View.VISIBLE);
				
				if (s.length() > 3)
				{
					
					mMapsLayout.setVisibility(View.GONE);
					
					if (mHandler != null)
					{
						mHandler.removeCallbacksAndMessages(null);
					}
					
					mHandler = new Handler();
					
					mHandler.postDelayed(new Runnable()
					{
						@Override
						public void run() {
							
							String url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input="
									+ s +
									"&components=country:lk&key=" + getString(R.string.google_api_key);
							
							System.out.println(url);
							
							VolleyWorker.makeJSONObjectRequest(OriginSearchActivity.this, url, new VolleyResponseListener()
							{
								@Override
								public void onResponse(Object response) {
									
									ArrayList<LocationItem> items = new ArrayList<>();
									
									// clear the adapter
									mOriginLocationAdapter.clear();
									
									mOriginLocationList.clear();
									
									JSONObject json = (JSONObject) response;
									
									try
									{
										
										JSONArray predictions = json.getJSONArray("predictions");
										
										for (int i = 0; i < predictions.length(); i++)
										{
											
											JSONObject jsonObject = predictions.getJSONObject(i);
											
											String location = jsonObject.getJSONObject("structured_formatting")
													.get("main_text").toString();
											
											System.out.println(location);
											
											String location_2 = jsonObject.getJSONObject("structured_formatting")
													.get("secondary_text").toString();
											
											String place_id = jsonObject.get("place_id")
													.toString();
											
											LocationItem item = new LocationItem(location, location_2, place_id);
											
											items.add(item);
											
										}
										
										mOriginLocationList.addAll(items);
										
										mOriginLocationAdapter.notifyDataSetChanged();
										
										mProgressBar.setVisibility(View.GONE);
										
										
									} catch (JSONException e)
									{
										e.printStackTrace();
									}
									
								}
								
								@Override
								public void onError(Object error) {
								
								}
							});
							
						}
					}, 1000);
					
				}
				else
				{
					
					// hide the progress bar
					if (s.length() < 1)
					{
						
						startActivity(new Intent(OriginSearchActivity.this, MapsActivity.class));
						
						// mProgressBar.setVisibility(View.GONE);
						
						// mListViewOrigin.setVisibility(View.GONE);
						
						// mMapsLayout.setVisibility(View.VISIBLE);
					}
					
					mOriginLocationList.clear();
					
					mOriginLocationAdapter.clear();
					
					mOriginLocationAdapter.notifyDataSetInvalidated();
				}
				
				
				return false;
			}
		});
		
	}
	
	private void listViewClickListeners() {
		
		
		this.mListViewOrigin.setOnItemClickListener(new AdapterView.OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				
				mProgressBar.setVisibility(View.GONE);
				
				LocationItem det = (LocationItem) mOriginLocationAdapter.getItem(i);
				
				final String url = "https://maps.googleapis.com/maps/api/geocode/json?place_id="
						+ det.getPlaceId() +
						"&key=" + getString(R.string.google_api_key);
				
				VolleyWorker.makeJSONObjectRequest(OriginSearchActivity.this, url, new VolleyResponseListener()
				{
					@Override
					public void onResponse(Object response) {
						
						JSONObject json = (JSONObject) response;
						
						try
						{
							JSONObject place = json.getJSONArray("results")
									.getJSONObject(0)
									.getJSONObject("geometry")
									.getJSONObject("location");
							
							origin = new LatLng(place.getDouble("lat"), place.getDouble("lng"));
							
							Bundle args = new Bundle();
							
							args.putParcelable("origin", origin);
							
							Intent intent = new Intent(OriginSearchActivity.this, DestinationSearchActivity.class);
							
							intent.putExtra("bundle", args);
							
							startActivity(intent);
							
						} catch (JSONException e)
						{
							e.printStackTrace();
						}
						
					}
					
					@Override
					public void onError(Object error) {
					
					}
				});
				
			}
		});
		
	}
	
	private void markOriginOnMap() {
		Toast.makeText(this, "Origin Selected", Toast.LENGTH_LONG).show();
//
//		mProgressBar.setVisibility(View.GONE);
//
//		mListViewOrigin.setVisibility(View.GONE);
//
//		mMapsLayout.setVisibility(View.VISIBLE);
	}
	
	
	@Override
	public void onMapReady(GoogleMap googleMap) {
		mMap = googleMap;
	}
}
